extends StaticBody2D

onready var sprite = get_node("Sprite")
onready var spwntimer = get_node("Timer")
onready var hud = get_node("/root/Stage/HUD")

export var type = ''
export var width = 0
export var height = 0
export var env = 'Normal'
var amount = 100
var environments = {"Normal":0,"Ice":3,"Fire":6}
func _ready():
	set_process_input(true)
	$Sprite.frame = environments[env]

func getAmount():
	return amount

func setAmount(val):
	spwntimer.start()
	amount += val
	if amount <= 60 && amount > 20:
		$Sprite.frame = environments[env] + 1
	if amount < 20:
		$Sprite.frame = environments[env] + 2



func _on_Timer_timeout():
	if amount <= 40:
		amount += 60
	else:
		amount = 100
	sprite.frame = environments[env]

func _on_AreaResource_mouse_entered():
	var label = [type,"Quantity:"+str(amount),"A resource, press space to collect it"]
	hud.setDescription(sprite,label)
	
func _on_AreaResource_mouse_exited():
	hud.hideDescription()
	hud.locked = false

func _on_AreaResource_input_event(viewport, event, shape_idx):
	if Input.is_mouse_button_pressed(BUTTON_LEFT):
		var label = [type,"Quantity:"+str(amount),"A resource, press space to collect it"]
		hud.setDescription($Sprite,label)
		hud.locked = !hud.locked
		print(hud.locked)
		
func on_click():
	print("Click")
