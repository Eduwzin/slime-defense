extends StaticBody2D

export var maxMoney = 1000

var money

func _ready():
	money = maxMoney
	update()

func add_money(amount):
	money = money + amount if (amount + money) < maxMoney else maxMoney
	update()
	
func update():
	$Money.text = str(money)
	$Sprite.frame = ceil(3.0*money/maxMoney)

func remove_money(amount):
	var removed = money if money < amount else amount
	money -= removed
	update()
	return removed

func get_money():
	return money