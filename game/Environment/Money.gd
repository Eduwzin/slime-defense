extends "res://Environment/Resource.gd"


func _on_AreaResource_area_entered(area):
	if area.is_in_group("MoneyArea"):
		if !is_queued_for_deletion():
			var din = area.get_parent()
			setAmount(din.getAmount())
			din.queue_free()

