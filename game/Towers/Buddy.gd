extends KinematicBody2D

onready var sprite = $Sprite
onready var ap = $Sprite/AnimationPlayer
onready var timer = $AttackTime
onready var slime = get_node("/root/Stage/Wall/Slime")
onready var hud = get_node("/root/Stage/HUD")

const anim_dir = ["Right", "Up", "Left", "Down"]

export (int) var speed = 200
export var ground = false
export var air = false
export var aspd = 0.0
export var damage = 0
export var type = 'single'
export var stones = 0
export var leaves = 0
export var fire = 0
export var tname = "Buddy"
export(String, MULTILINE) var description = "Descrição: Um companheiro."
export var block_graph = false

var velocity = Vector2()
var anim = 3
var moving = false
var radius = 0
var vision = 0
var target = null
var targets = []
var moneytargets = []
var attacking = false
var can_attack = true
var spot = Vector2()
var info = ""
var label = []

func get_side(vec):
	if abs(vec.x) < abs(vec.y):
		if vec.y > 0:
			return 3
		elif vec.y <= 0:
			return 1
	else:
		if vec.x > 0:
			return 0
		elif vec.x <= 0:
			return 2

func _ready():
	timer.wait_time = INF if aspd == 0 else 1/aspd
	radius = $RangeArea/CollisionShape2D.shape.radius + 5
	vision = $VisionArea/CollisionShape2D.shape.radius + 5
	$Range.scale.x = radius/128
	$Range.scale.y = radius/128
	info = "ASPD:"+str(aspd)+"\nRange:"+str(radius)+"\nDamage:"+str(damage)+"\nType:"+str(type)
	spot = position
	label = [tname,info,description]
	$Sprite/AnimationPlayer.play("IdleDown")

func _process(delta):
	if attacking:
		velocity = Vector2()
	else:
		if not target or not target.get_ref() or \
		   not target.get_ref().has_method("damage") or \
		   position.distance_to(target.get_ref().position) > vision:
			target = null
			# Escolhe um target, priorizando os que tem dinheiro e estão a mais tempo na area
			if not moneytargets.empty():
				target = weakref(moneytargets.pop_front())
			elif not targets.empty():
				target = weakref(targets.pop_front())
			else:
				# Não achou target, volta para a base
				if position.distance_to(spot) < 5:
					velocity = Vector2()
				else:
					velocity = spot - position
		else:
			# Persegue o target
			var target_pos = target.get_ref().position
			if position.distance_to(target_pos) < radius:
				velocity = Vector2()
				if can_attack:
					$Sprite/AnimationPlayer.play("Attack" + anim_dir[get_side(target_pos - position)])
					attacking = true
					can_attack = false
					attack()
					timer.start()
			else:
				velocity = target_pos - position

	if not attacking:
		# Muda a animação
		var correct_anim = get_side(velocity)
		if not velocity and moving:
			ap.play("Idle" + anim_dir[anim])
			moving = false
		elif velocity and (anim != correct_anim or not moving):
			anim = correct_anim
			moving = true
			ap.play("Walk" + anim_dir[anim])

func _physics_process(delta):
	move_and_slide(velocity.normalized() * speed)
	$VisionArea.position = spot - position

func _on_AttackTime_timeout():
	can_attack = true

func attack():
	pass

func cost():
	return {"Stone":stones,"Leaf":leaves,"Fire":fire}

func _on_ActionArea_input_event(viewport, event, shape_idx):
	if Input.is_mouse_button_pressed(BUTTON_LEFT):
		var label = [tname,info,description]
		hud.setDescription(sprite,label)
		hud.locked = !hud.locked

func _on_ActionArea_mouse_entered():
	var label = [tname,info,description]
	$Range.show()
	hud.setDescription(sprite,label)

func _on_ActionArea_mouse_exited():
	hud.hideDescription()
	$Range.hide()
	hud.locked = false

func _on_VisionArea_body_entered(body):
	if body.is_in_group("Monster"):
		if body.hasMoney:
			moneytargets.push_back(body)
		else:
			targets.push_back(body)

func _on_VisionArea_body_exited(body):
	if body.is_in_group("Monster"):
		if body.hasMoney:
			moneytargets.erase(body)
		else:
			targets.erase(body)
		if target and target.get_ref() == body:
			target = null

func _on_AnimationPlayer_animation_finished(anim_name):
	if "Attack" in anim_name:
		attacking = false
