extends "res://Towers/Tower.gd"

onready var FireBullet = preload("res://Bullets/FireBullet/FireBullet.tscn")

func attack():
	var new_fb = FireBullet.instance()
	new_fb.set_target(target)
	new_fb.position = position
	new_fb.damage = damage
	get_parent().add_child(new_fb)