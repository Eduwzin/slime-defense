extends "res://Towers/Tower.gd"

onready var ExplosionBullet = preload("res://Bullets/ExplosionBullet/ExplosionBullet.tscn")

func attack():
	var new_eb = ExplosionBullet.instance()
	new_eb.set_target(target)
	new_eb.position = position
	new_eb.damage = damage
	get_parent().add_child(new_eb)
	$SmokeParticles.restart()