extends "res://Towers/Buddy.gd"

onready var StoneBullet = preload("res://Bullets/StoneBullet/StoneBullet.tscn")

func attack():
	var new_sb = StoneBullet.instance()
	new_sb.set_target(target)
	new_sb.damage = damage
	new_sb.position = position + Vector2(0, -8)
	get_parent().add_child(new_sb)