extends StaticBody2D

export var ground = false
export var air = false
export var aspd = 0.0
export var damage = 0
export var type = 'single'
export var stones = 0
export var leaves = 0
export var fire = 0
export var block_graph = true
export var tname = ""
export(String, MULTILINE) var description = ""

onready var timer = $Timer
onready var sprite = $Sprite
onready var hud = get_node("/root/Stage/HUD")
onready var slime = get_node("/root/Stage/Wall/Slime")

var info = ""
var label = []
var radius = 0
var target = null
var targets = []
var moneytargets = []
var direction = Vector2()
#var directions = ["left", "up_left", "up", "up_right", "right", "down_right", "down", "down_left"]
var directions = [6,4,5,3,7,1,0,2]

func _ready():
	timer.wait_time = 1/aspd
	timer.start()
	radius = $Area2D/CollisionShape2D.shape.radius + 5
	$Range.scale.x = radius/128
	$Range.scale.y = radius/128
	info = "ASPD:"+str(aspd)+"\nRange:"+str(radius)+"\nDamage:"+str(damage)+"\nType:"+str(type)
	label = [tname,info,description]

func _process(delta):
	if not target or not target.get_ref() or \
	   not target.get_ref().has_method("damage") or \
	   position.distance_to(target.get_ref().position) > radius:
		target = null
		# Escolhe um target, priorizando os que tem dinheiro e estão a mais tempo na area
		if not moneytargets.empty():
			target = weakref(moneytargets.pop_front())
		elif not targets.empty():
			target = weakref(targets.pop_front())
	else:
		var targ_pos = target.get_ref().position
		var direction = position - targ_pos
		var distance = direction.length()
		direction = direction.normalized()
		var dir = direction2str(direction)
		$Sprite.frame = int(dir)

func _on_Area2D_body_entered(body):
	if body.is_in_group("Monster"):
		if body.hasMoney:
			moneytargets.push_back(body)
		else:
			targets.push_back(body)

func _on_Area2D_body_exited(body):
	if body.is_in_group("Monster"):
		if body.hasMoney:
			moneytargets.erase(body)
		else:
			targets.erase(body)

func _on_Timer_timeout():
	if target != null:
		if target.get_ref():
			attack()
		else:
			target = null

func attack():
	pass

func _on_ActionArea_mouse_exited():
	hud.hideDescription()
	$Range.hide()
	hud.locked = false

func _on_ActionArea_mouse_entered():
	var label = [tname,info,description]
	$Range.show()
	hud.setDescription(sprite,label)

func destroy():
	queue_free()

func cost():
	return {"Stone":stones,"Leaf":leaves,"Fire":fire}

func _on_ActionArea_input_event(viewport, event, shape_idx):
	if Input.is_mouse_button_pressed(BUTTON_LEFT):
		var label = [tname,info,description]
		hud.setDescription(sprite,label)
		hud.locked = !hud.locked
		
func direction2str(direction):
	var angle = direction.angle()
	if angle < 0:
		angle += 2 * PI
	var index = round(angle / PI * 4)
	index = 7 if index > 7 else index 
	return directions[index]
