extends Area2D

var label = []

func _ready():
	$Particles2D.emitting = true
	label = ["Special", "Speed mod: 0.8", "The slime leaves a sticky goo behind its path to slow enemies"]

func _on_Timer_timeout():
	get_node("/root/Stage").remove_special(position)
	queue_free()


func reset_timer():
	$Timer.start()
	$Particles2D.restart()


func _on_Special_body_entered(body):
	if body.is_in_group("Monster") and not body.flying:
		body.speed *= 0.8


func _on_Special_body_exited(body):
	if body.is_in_group("Monster") and not body.flying:
		body.speed *= 1.25
