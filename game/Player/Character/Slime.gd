extends KinematicBody2D

onready var animation = get_node("Slimeanim")
onready var hud = get_node("/root/Stage/HUD")
onready var stage = get_node("/root/Stage")

export (int) var speed = 150

const towers = ["ShootBuddy", "SwordBuddy", "StoneTower",
				"SleepBuddy", "FireTower"]

var velocity = Vector2()
var animtoplay = "WalkDown"
var resourceareas = []
var towerareas = []
var onChest = false
var selAction = 1
var areasAtActionArea = 0
var blockActionArea = false
var gettingResources = false
var lastActionAnim = "ActionDown"
#resources ?
var leaves = 0
var stones = 0
var fire = 0
var money  = 0


func _ready():
	var camBound = stage.get_node("CameraBound")
	setActionAreaPos(0, 1)
	animation.stop()
	$Camera2D.limit_left = 0
	$Camera2D.limit_top = 0
	$Camera2D.limit_right = camBound.position.x
	$Camera2D.limit_bottom = camBound.position.y

func _process(delta):
	velocity = Vector2()
	
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
		animtoplay = "WalkRight"
		gettingResources = false
	
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
		animtoplay = "WalkLeft"
		gettingResources = false
	
	if Input.is_action_pressed("ui_up"):
		velocity.y -= 1
		animtoplay = "WalkUp"
		gettingResources = false
	
	if Input.is_action_pressed("ui_down"):
		velocity.y += 1
		animtoplay = "WalkDown"
		gettingResources = false
	
	if Input.is_action_just_pressed("ui_z"):
		if not $ActionArea/Sprite.visible:
			hud.block_selection(true)
			selAction = hud.sel
			if selAction != 6:
				updateActionAreaState()
				$ActionArea/Sprite.visible = true
				return
		if not blockActionArea:
			var spawn_pos = $ActionArea.global_position
			stage.add_tower(towers[selAction - 1], spawn_pos)
		hud.block_selection(false)
		$ActionArea/Sprite.visible = false
	
	if Input.is_action_pressed("ui_z"):
		if selAction == 6:
			stage.summon_special(position)
	
	if Input.is_action_just_pressed("ui_x"):
		if $ActionArea/Sprite.visible:
			$ActionArea/Sprite.visible = false
	
	if not gettingResources and (Input.is_action_just_pressed("ui_accept") or \
		(Input.is_action_pressed("ui_accept") and not velocity)):
		if not resourceareas.empty():
			animtoplay = actionAnim()
			animation.play(animtoplay)
			gettingResources = true
		if onChest:
			stage.returnMoney(money)
			self.getMoney(money)
	
	velocity = velocity.normalized() * speed
	if (animation.assigned_animation != animtoplay):
		animation.play(animtoplay)
		
	if velocity == Vector2() and not animation.is_playing():
		animation.stop()
	elif !animation.is_playing():
		animation.play()


func _physics_process(delta):
	move_and_slide(velocity)
	match animtoplay:
		"WalkRight":
			setActionAreaPos(1, 0)
			setSlimeAreaRot(270)
		"WalkLeft":
			setActionAreaPos(-1, 0)
			setSlimeAreaRot(90)
		"WalkUp":
			setActionAreaPos(0, -1)
			setSlimeAreaRot(180)
		"WalkDown":
			setActionAreaPos(0, 1)
			setSlimeAreaRot(0)


func setLeaf(value):
	leaves += value
	hud.get_node("Leaves").text = str(leaves)


func setStone(value):
	stones += value
	hud.get_node("Stones").text = str(stones)


func setFire(value):
	fire += value
	hud.get_node("Fire").text = str(fire)


func setMoney(value):
	money += value
	hud.get_node("Money").text = str(money)

func resources():
	return {"Stone":stones,"Leaf":leaves,"Fire":fire}


func getMoney(value):
	var removed = money if money < value else value
	money -= removed
	hud.get_node("Money").text = str(money)
	return removed


func get_money():
	return money


func getResource():
	var resource = null
	var mindist = INF
	for rsrc in resourceareas:
		var dist = self.position.distance_to(rsrc.position)
		if dist < mindist:
			mindist = dist
			resource = rsrc
	if resource.type != "Money":
		if resource.amount >= 20:
			resource.setAmount(-20)
			print(resource.type)
			funcref(self,'set'+resource.type).call_func(20)
	else:
		funcref(self,'set'+resource.type).call_func(resource.amount)
		resource.queue_free()
		print("Peguei o dinhero")


func actionAnim():
	var currentanim = animation.assigned_animation
	if(currentanim == "WalkLeft"):
		return "ActionLeft"
	elif(currentanim == "WalkRight"):
		return "ActionRight"
	elif(currentanim == "WalkDown"):
		return "ActionDown"
	elif(currentanim == "WalkUp"):
		return "ActionUp"
	else:
		return animation.assigned_animation


func updateActionAreaState():
	if areasAtActionArea == 0 and stage.can_put_tower($ActionArea.global_position):
		var Tower = load("res://Towers/" + towers[hud.sel - 1] + ".tscn")
		var tower = Tower.instance()
		if not stage.slime_can_buy(self, tower):
			$ActionArea/Sprite.modulate = Color(1, 1, 0)
			blockActionArea = true
		else:
			$ActionArea/Sprite.modulate = Color(0, 1, 0)
			blockActionArea = false
		tower.free()
	else:
		$ActionArea/Sprite.modulate = Color(1, 0, 0)
		blockActionArea = true


func setActionAreaPos(x, y):
	var ts = stage.tileSize
	var roundX = ts*(round(position.x/ts) + 3*x) - position.x
	var roundY = ts*(round(position.y/ts) + 3*y) - position.y
	var oldPos = $ActionArea.position
	$ActionArea.position = Vector2(roundX, roundY)
	if (oldPos - $ActionArea.position).length() >= ts/2 and $ActionArea/Sprite.visible:
		updateActionAreaState()


func setSlimeAreaRot(angle):
	$SlimeArea.rotation_degrees = angle


func _on_SlimeArea_area_entered(area):
	if area.is_in_group("ChestArea"):
		onChest = true
	if area.is_in_group("ResourceArea"):
		resourceareas.push_back(area.get_parent())
	if area.is_in_group("TowerActionArea"):
		towerareas.push_back(area.get_parent())


func _on_SlimeArea_area_exited(area):
	if area.is_in_group("ChestArea"):
		onChest = false
	if area.is_in_group("ResourceArea"):
		resourceareas.erase(area.get_parent())
	if area.is_in_group("TowerActionArea"):
		towerareas.erase(area.get_parent())


func _on_ActionArea_body_entered(body):
	if body.is_in_group("Wall") or body.is_in_group("Deco"):
		areasAtActionArea += 1


func _on_ActionArea_body_exited(body):
	if body.is_in_group("Wall") or body.is_in_group("Deco"):
		areasAtActionArea -= 1


func _on_Slimeanim_animation_finished(anim_name):
	if "Action" in anim_name:
		if not resourceareas.empty():
			getResource()
			lastActionAnim = anim_name
			$Cooldown.start()


func _on_Cooldown_timeout():
	if Input.is_action_pressed("ui_accept") and not resourceareas.empty():
		$Slimeanim.play(lastActionAnim)
	else:
		gettingResources = false
