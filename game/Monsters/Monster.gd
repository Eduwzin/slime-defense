extends KinematicBody2D

onready var ap = $Sprite/AnimationPlayer
onready var hud = get_node("/root/Stage/HUD")
onready var Money = preload("res://Environment/Money.tscn")
onready var stage = get_node("/root/Stage")

var label = ""
var anim = 3
var velocity = Vector2()
var hasMoney = false

export var type = ""
export var speed = 50
export var flying = false
export var life = 100
export var pouch_size = 100
export(String, MULTILINE) var description = ""
export(int) var spawnId

func _ready():
	label = [type,"Life:"+str(life)+"\nSpeed:"+str(speed),description]
	$ProgressBar.max_value = life
	$ProgressBar.value = life

func _process(delta):
	if hasMoney:
		velocity = speed * stage.get_spawn_field(position, spawnId)
	else:
		velocity = speed * stage.get_chest_field(position)
	if abs(velocity.x) < abs(velocity.y):
		if velocity.y > 0 and anim != 3:
			anim = 3
			ap.play("WalkDown")
		elif velocity.y <= 0 and anim != 1:
			anim = 1
			ap.play("WalkUp")
	else:
		if velocity.x > 0 and anim != 0:
			anim = 0
			ap.play("WalkRight")
		elif velocity.x <= 0 and anim != 2:
			anim = 2
			ap.play("WalkLeft")

func _physics_process(delta):
	move_and_slide(velocity)

func damage(ammount):
	life -= ammount
	$ProgressBar.value = life
	if life <= 0:
		if hasMoney:
			var money = Money.instance()
			money.position = self.position
			money.amount = pouch_size
			get_parent().add_child(money)
		queue_free()

func _on_Area2D_area_entered(area):
	if area.is_in_group("ChestArea"):
		stage.getMoney(pouch_size)
		hasMoney = true
		$ProgressBar/hasMoney.show()
		
	elif area.is_in_group("SpawnerArea") and hasMoney:
		queue_free()

func _on_Area2D_input_event(viewport, event, shape_idx):
	if event.is_action_pressed("inspect"):
		hud.setDescription($Sprite, label)

	

func _on_Area2D_mouse_entered():
	#var label = [type,"Life:"+str(life)+"\nSpeed:"+str(speed),"Descrição: Bla bla blba balb albalba"]
	#hud.setDescription($Sprite,label)
	pass

func _on_Area2D_mouse_exited():
	#hud.hideDescription()
	pass
