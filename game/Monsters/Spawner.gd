extends Node2D

onready var stage = get_node("/root/Stage")

export(int) var controlId
export(Vector2) var clampedPos

func _ready():
	var bounds = stage.get_node("CameraBound").position
	clampedPos = Vector2(clamp(position.x, 0, bounds.x - 1), clamp(position.y, 0, bounds.y - 1))

func spawn(monster):
	var randVec = 32*Vector2(randf() - 0.5, randf() - 0.5)
	var new_mon = load("res://Monsters/" + monster + ".tscn").instance()
	new_mon.position = position + randVec
	new_mon.spawnId = controlId
	get_parent().add_child(new_mon)
