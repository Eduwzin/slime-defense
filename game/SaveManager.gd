extends Node

const savePath = "user://save.save"

var nextStage = {}
var initialStage = ""
var saveCache = null

func set_stage_order(stages):
	initialStage = stages[0]
	for i in range(stages.size() - 1):
		nextStage[stages[i]] = stages[i+1]
	nextStage[stages[stages.size() - 1]] = "__END__"

func exist_save():
	return File.new().file_exists(savePath)

func new_save():
	var save = {}
	var stage = initialStage
	while stage != "__END__":
		save[stage] = {
			"enabled": false,
			"score": -1
		}
		stage = nextStage[stage]
	save[initialStage]["enabled"] = true
	override_save(save)
	return save

func load_save():
	if saveCache == null:
		var saveFile = File.new()
		if saveFile.file_exists(savePath):
			saveFile.open(savePath, saveFile.READ)
			saveCache = JSON.parse(saveFile.get_as_text()).result
		else:
			saveCache = new_save()
		saveFile.close()
	return saveCache

func override_save(newSave):
	var saveFile = File.new()
	saveFile.open(savePath, saveFile.WRITE)
	saveFile.store_string(JSON.print(newSave))
	saveCache = newSave

func delete_save():
	var dir = Directory.new()
	dir.remove(savePath)
	saveCache = null

func save_stage(stage, score):
	var save = load_save()
	save[stage]["score"] = max(score, save[stage]["score"])
	if nextStage[stage] != "__END__":
		save[nextStage[stage]]["enabled"] = true
	override_save(save)

func get_stage(name):
	var save = load_save()
	if not (name in save):
		return {}
	return save[name]
