extends Control

func _ready():
	$VBoxContainer/NewGame.grab_focus()
	randomize()
	if not get_node("/root/global").exist_save():
		$VBoxContainer/LoadGame.disabled = true
		$VBoxContainer/LoadGame.focus_mode = FOCUS_NONE

func _on_NewGame_pressed():
	get_node("/root/global").delete_save()
	get_tree().change_scene("res://Menus/StageSelection/StageSelection.tscn")

func _on_LoadGame_pressed():
	get_tree().change_scene("res://Menus/StageSelection/StageSelection.tscn")

func _on_Credits_pressed():
	get_tree().change_scene("res://Menus/Credits/Credits.tscn")

func _on_Quit_pressed():
	get_tree().quit()

func _on_Controls_pressed():
	get_tree().change_scene("res://Menus/Controls/Control.tscn")


