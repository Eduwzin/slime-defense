extends CanvasLayer

func _process(delta):
	if Input.is_action_just_pressed("ui_esc"):
		$Pause.visible = not $Pause.visible
		get_tree().paused = not get_tree().paused
		$Pause/VBoxContainer/Resume.grab_focus()

func _on_Resume_pressed():
	$Pause.visible = false
	get_tree().paused = false

func _on_Restart_pressed():
	get_tree().reload_current_scene()
	get_tree().paused = false

func _on_Exit_pressed():
	get_tree().change_scene("res://Menus/StageSelection/StageSelection.tscn")
	get_tree().paused = false
