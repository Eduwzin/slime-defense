extends CanvasLayer

func appear():
	$Control.show()
	$Control/VBoxContainer/Restart.grab_focus()

func _on_Restart_pressed():
	get_tree().reload_current_scene()
	get_tree().paused = false

func _on_Exit_pressed():
	get_tree().change_scene("res://Menus/StageSelection/StageSelection.tscn")
	get_tree().paused = false