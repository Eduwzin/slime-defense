extends CanvasLayer

func appear(stage, percentage, saved, total):
	get_tree().paused = true
	var savedInfo = get_node("/root/global").get_stage(stage)
	var oldBest = savedInfo["score"]
	var newBest = percentage if percentage > oldBest else oldBest
	$Control/Stats.text = "Best: "+"%.2f"%newBest+"%\nActual: "+"%.2f"%percentage+"%\nYou saved "+str(saved)+"$ of "+str(total)+"$"
	$Control.show()
	$Control/VBoxContainer/Restart.grab_focus()

func _on_Restart_pressed():
	get_tree().reload_current_scene()
	get_tree().paused = false

func _on_Exit_pressed():
	get_tree().change_scene("res://Menus/StageSelection/StageSelection.tscn")
	get_tree().paused = false