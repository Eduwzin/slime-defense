extends Control
onready var slime = $Container/MovContainer/Slime
func _ready():
	$Container/Back.grab_focus()

func _on_Back_pressed():
	get_tree().change_scene("res://Menus/MainMenu/MainMenu.tscn")

func _process(delta):
	
	if Input.is_action_pressed("ui_right"):
		slime.frame = 21
		
	if Input.is_action_pressed("ui_left"):
		slime.frame = 14
	
	if Input.is_action_pressed("ui_up"):
		slime.frame = 7
	
	if Input.is_action_pressed("ui_down"):
		slime.frame = 0