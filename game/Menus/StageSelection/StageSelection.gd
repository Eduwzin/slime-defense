extends Control

var stages = []

func _ready():
	var stagesNames = []
	for child in get_children():
		if child.is_in_group("Stage"):
			stages.append(child)
			stagesNames.append(child.stage_name)
	stages[0].enable()
	stages[0].get_node("Button").grab_focus()
	get_node("/root/global").set_stage_order(stagesNames)
	
	var save = get_node("/root/global").load_save()

	for stage in stages:
		if stage.stage_name in save:
			if save[stage.stage_name]["enabled"]:
				stage.enable()
			stage.set_score(save[stage.stage_name]["score"])

func _process(delta):
	if Input.is_action_just_pressed("ui_esc"):
		get_tree().change_scene("res://Menus/MainMenu/MainMenu.tscn")
	# TODO: Remove this
	if Input.is_action_just_pressed("ui_z"):
		for stage in stages:
			stage.enable()

func _on_stage_selected(stage_name):
	get_tree().change_scene("res://Stages/" + stage_name + "/" + stage_name + ".tscn")
