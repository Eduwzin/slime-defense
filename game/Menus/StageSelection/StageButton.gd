extends HBoxContainer

signal selected

export(String) var stage_name
export(String) var title

var score = -1

func _ready():
	$Label.text = title

func enable():
	$Button.disabled = false
	$Button.focus_mode = FOCUS_ALL

func is_enabled():
	return not $Button.disabled

func set_score(newScore):
	score = newScore
	if score != -1:
		$Label/Label2.show()
		$Label/Label2.text = "Best %.2f%%" % score

func _on_Button_focus_entered():
	$Label.visible = true

func _on_Button_focus_exited():
	$Label.visible = false

func _on_Button_pressed():
	emit_signal("selected", stage_name)
