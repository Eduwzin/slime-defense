extends CanvasLayer

onready var selected = get_node("SummonBar/Select")
onready var desc     = get_node("Description")
onready var descimage = get_node("Description/Image")

var sel = 1
var locked = false
var selectionBlocked = false

func _input(event):
	if not selectionBlocked:
		if   Input.is_key_pressed(KEY_1):
			sel = 1
		elif Input.is_key_pressed(KEY_2):
			sel = 2
		elif Input.is_key_pressed(KEY_3):
			sel = 3
		elif Input.is_key_pressed(KEY_4):
			sel = 4
		elif Input.is_key_pressed(KEY_5):
			sel = 5
		elif Input.is_key_pressed(KEY_6):
			sel = 6
		selected.position.x = (sel-1)*32 + 17

func setDescription(sprite,label):
	desc.show()
	descimage.texture = sprite.texture
	descimage.vframes = sprite.vframes
	descimage.hframes = sprite.hframes
	descimage.frame = sprite.frame
	for i in len(label):
		get_node("Description/Label"+str(i+1)).text = label[i]

func hideDescription():
	if(!locked):
		desc.hide()

func change_wave(wave):
	$Wave.text = "Wave " + str(wave)

func wait_for_next_wave(nextWave):
	$Wave.text = "Waiting for wave " + str(nextWave)

func block_selection(cannotSelect):
	selectionBlocked = cannotSelect

func _on_Close_pressed():
	desc.hide()

func _on_Summon_mouse_exited():
	hideDescription()
	locked = false

func _on_Summon_pressed(number):
	var summon = get_node("SummonBar/Summon"+str(number))
	selected.position.x = summon.rect_position.x + (summon.rect_size.x)/2
	selected.position.y = summon.rect_position.y + (summon.rect_size.y)/2 
	sel = number
	locked = !locked


func _on_Summon_mouse_entered(number,path):
	var Summon = load(path)
	var summon = Summon.instance()
	add_child(summon)
	var child_name = "SummonBar/Summon" + str(number) + "/Sprite"
	setDescription(get_node(child_name),summon.label)
	summon.queue_free()



func _on_Summon5_mouse_entered():
	pass # replace with function body


func _on_Summon6_mouse_entered():
	pass # replace with function body
