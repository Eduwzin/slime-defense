extends KinematicBody2D

export var speed = 10
export var damage = 40

var target = null

func _ready():
	pass

func _process(delta):
	if not target.get_ref():
		queue_free()

func _physics_process(delta):
	if target.get_ref():
		var dir = target.get_ref().position - position - Vector2(0, 7)
		move_and_collide(speed*dir.normalized())

func set_target(_target):
	target = _target

func _on_Area2D_body_entered(body):
	if body.is_in_group("Monster") and body == target.get_ref():
		body.damage(damage)
		queue_free()
