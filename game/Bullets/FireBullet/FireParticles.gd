extends Node2D

var damage = 2

func reset():
	$DeathTimer.start()


func _on_DamageTimer_timeout():
	get_parent().damage(damage)


func _on_DeathTimer_timeout():
	queue_free()