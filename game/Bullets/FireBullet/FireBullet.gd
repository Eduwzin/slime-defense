extends "res://Bullets/Bullet.gd"

onready var FireParticles = preload("res://Bullets/FireBullet/FireParticles.tscn")

func _on_Area2D_body_entered(body):
	if body.is_in_group("Monster") and body == target.get_ref():
		body.damage(damage)
		if body.has_node("FireParticles"):
			body.get_node("FireParticles").reset()
		else:
			body.add_child(FireParticles.instance())
		queue_free()