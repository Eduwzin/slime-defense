extends "res://Bullets/Bullet.gd"


var targets = []
var targetpos = Vector2()

	
func _process(delta):
	if target.get_ref():
		var dir = (target.get_ref().position - position).normalized()
		position += speed*dir
	else:
		queue_free()

func damage():
	for body in targets:
		print(body)
		if !body.is_queued_for_deletion():
			body.damage(damage)
	queue_free()



func _on_Area2D_body_entered(body):
	if body.is_in_group("Monster"):
		damage()


func _on_ExplosionArea_body_entered(body):
	if body.is_in_group("Monster"):
		targets.push_back(body)


func _on_ExplosionArea_body_exited(body):
	if body.is_in_group("Monster"):
		targets.erase(body)
