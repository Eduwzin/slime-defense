extends Position2D

func _ready():
	$InvisibleWall.global_position = Vector2(position.x + 16, position.y/2)
	$InvisibleWall.scale = Vector2(1, position.y/32)
	$InvisibleWall2.global_position = Vector2(position.x/2, -16)
	$InvisibleWall2.scale = Vector2(position.x/32, 1)
	$InvisibleWall3.global_position = Vector2(-16, position.y/2)
	$InvisibleWall3.scale = Vector2(1, position.y/32)
	$InvisibleWall4.global_position = Vector2(position.x/2, position.y + 16)
	$InvisibleWall4.scale = Vector2(position.x/32, 1)

