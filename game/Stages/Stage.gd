extends YSort

const tileSize = 16
const tileRatio = 0.5
const axisSuccs = [[1, 0], [0, 1], [-1, 0], [0, -1]]
const diagSuccs = [[1, 1], [1, -1], [-1, 1], [-1, -1]]
const diagNeighs = [[0, 1], [0, 3], [1, 2], [2, 3]]
const combs = [[0, 0], [0, 1], [1, 0], [1, 1]]

onready var Special = preload("res://Player/Special/Special.tscn")
onready var wall = $Wall
onready var fd = $FloorDecoration
onready var chest = $Wall/Chest
onready var spawner = $Wall/Spawner
onready var slime = $Wall/Slime

export var stageName = ""

var chestField = []
var spawnFields = []
var spawnerList = []
var hasTower = []
var hasSpecial = []
var as = AStar.new()
var auxAs = AStar.new()
var waves = []
var currentWave = 0
var currentMonster = 0
var fieldSize
var noMonstersAlive = true

func vec_to_pos(vec):
	return [int(vec.y/tileSize), int(vec.x/tileSize)]

func pos_to_id(pos):
	return pos[0]*fieldSize.x + pos[1]

func id_to_vec(id):
	return Vector2(id%int(fieldSize.x), int(id/fieldSize.x))

func id_to_pos(id):
	return [int(id/fieldSize.x), id%int(fieldSize.x)]

func vec_to_id(vec):
	return pos_to_id(vec_to_pos(vec))

func pos_to_cell_pos(pos):
	return tileSize*Vector2(pos[1], pos[0])

func signRange(x, a, b):
	return -1 if x < a else (0 if x < b else 1)

func shuffle_list(list):
	var l = list.size()
	for i in range(l - 1):
		var x = (randi()%(l - i)) + i
		var tmp = list[i]
		list[i] = list[x]
		list[x] = tmp

func random_list(maxVal, size):
	var list = []
	for i in range(size):
		list.append(round(100*maxVal*randf())/100)
	return list

func parse_waves(path):
	var file = File.new()
	file.open(path, file.READ)
	var json = JSON.parse(file.get_as_text()).result
	file.close()
	for wave in json:
		var duration = wave["__duration__"]
		var monsters = []
		wave.erase("__duration__")
		for key in wave.keys():
			for i in range(wave[key]):
				monsters.append(key)
		shuffle_list(monsters)
		var times = random_list(duration, monsters.size())
		times.sort()
		waves.append({
			"duration": duration,
			"monsters": monsters,
			"times": times
		})

func _ready():
	fieldSize = get_field_size()
	# Get references to spawners
	var id = 0
	for child in wall.get_children():
		if child.is_in_group("Spawner"):
			spawnerList.append(child)
			spawnFields.append([])
			child.controlId = id
			id += 1
	# Create empty matrices and A* vertices
	for i in range(fieldSize.y):
		chestField.append([])
		for k in range(spawnerList.size()):
			spawnFields[k].append([])
		hasTower.append([])
		hasSpecial.append([])
		for j in range(fieldSize.x):
			chestField[i].append(Vector2())
			for k in range(spawnerList.size()):
				spawnFields[k][i].append(Vector2())
			hasTower[i].append(false)
			hasSpecial[i].append(false)
			as.add_point(pos_to_id([i, j]), Vector3(i, j, 0))
			auxAs.add_point(pos_to_id([i, j]), Vector3(i, j, 0))
	# Create A* edges
	for i in range(fieldSize.y):
		for j in range(fieldSize.x):
			for succ in get_successors(i, j):
				var id1 = pos_to_id([i, j])
				var id2 = pos_to_id(succ)
				as.connect_points(id1, id2, false)
				auxAs.connect_points(id1, id2, false)
	# Process scenary
	for child in wall.get_children():
		if child.is_in_group("Resource"):
			remove_from_graph(child.position, [child.height, child.width], true)
			remove_from_aux_graph(child.position, [child.height, child.width])
	# Start waves
	parse_waves("res://Stages/" + stageName + "/waves.json")
	$NewWaveTimer.start()
	$HUD.wait_for_next_wave(currentWave + 1)

func _process(delta):
	var newNoMonstersAlive = true
	for child in wall.get_children():
		if child.is_in_group("Monster"):
			newNoMonstersAlive = false
			break
	if newNoMonstersAlive and currentMonster == 0 and \
	   $WaveTimer.is_stopped() and $NewWaveTimer.is_stopped():
		if currentWave == waves.size():
			# TODO: Change this to display win screen
			var totalMoney = float(slime.get_money() + chest.get_money())
			var percentage = 100.0*totalMoney/chest.maxMoney
			$Victory.appear(stageName,percentage,totalMoney,chest.maxMoney)
			get_node("/root/global").save_stage(stageName, percentage)
			currentWave = 0
		$NewWaveTimer.start()
		$HUD.wait_for_next_wave(currentWave + 1)
		print("New wave")
	noMonstersAlive = newNoMonstersAlive

func is_inside_field(i, j):
	return i < fieldSize.y and i >= 0 and j < fieldSize.x and j >= 0

func pos_is_free(i, j):
	var atWall = wall.get_cell(floor(j*tileRatio), floor(i*tileRatio))
	var atFD = fd.get_cell(floor(j*tileRatio), floor(i*tileRatio))
	return atWall == -1 and atFD == -1 and not hasTower[i][j]

func get_field_size():
	return $CameraBound.position/tileSize

func inside(l1, l2):
	for el in l1:
		if not (el in l2):
			return false
	return true

func get_successors(i, j, diag=true):
	var succs = []
	var blockedAxis = []
	for k in range(axisSuccs.size()):
		var vec = axisSuccs[k]
		var vi = i + vec[0]
		var vj = j + vec[1]
		if is_inside_field(vi, vj):
			if pos_is_free(vi, vj):
				succs.append([vi, vj])
			else:
				blockedAxis.append(k)
	if diag:
		for k in range(diagSuccs.size()):
			var vec = diagSuccs[k]
			var vi = i + vec[0]
			var vj = j + vec[1]
			if is_inside_field(vi, vj) and pos_is_free(vi, vj) \
			   and not inside(diagNeighs[k], blockedAxis):
				succs.append([vi, vj])
	return succs

func clear_fields():
	for i in range(fieldSize.y):
		for j in range(fieldSize.x):
			chestField[i][j] = Vector2()
			for k in range(spawnerList.size()):
				spawnFields[k][i][j] = Vector2()

func update_field(field, beg, end):
	var path = as.get_id_path(vec_to_id(beg), vec_to_id(end))
	if path.size() > 1:
		var pos1
		var pos2 = id_to_pos(path[0])
		for i in range(path.size()-1):
			pos1 = pos2
			pos2 = id_to_pos(path[i+1])
			if field[pos1[0]][pos1[1]]:
				return
			field[pos1[0]][pos1[1]] = Vector2(pos2[1] - pos1[1], pos2[0] - pos1[0]).normalized()
		field[pos2[0]][pos2[1]] = field[pos1[0]][pos1[1]]

func slime_can_buy(slime, tower):
	var costs = tower.cost()
	var resources = slime.resources()
	return costs["Stone"] <= resources["Stone"] && \
		   costs["Leaf"] <= resources["Leaf"] && \
		   costs["Fire"] <= resources["Fire"]

func add_tower(type, vec):
	var Tower = load("res://Towers/" + type + ".tscn")
	var tower = Tower.instance()
	if slime_can_buy(slime, tower):
		var costs = tower.cost()
		slime.setStone(-costs["Stone"])
		slime.setFire(-costs["Fire"])
		slime.setLeaf(-costs["Leaf"])
		tower.position = vec
		wall.add_child(tower)
		remove_from_graph(vec, [32, 32], tower.block_graph)
		if tower.block_graph:
			remove_from_aux_graph(vec, [32, 32])
	else:
		tower.free()
		print("RECURSOS INSUFICIENTES")
	

func summon_special(vec):
	var pos = vec_to_pos(vec)
	if not hasSpecial[pos[0]][pos[1]]:
		var new_spec = Special.instance()
		new_spec.position = pos_to_cell_pos(pos)
		$FloorDecoration.add_child(new_spec)
		hasSpecial[pos[0]][pos[1]] = new_spec
	else:
		hasSpecial[pos[0]][pos[1]].reset_timer()

func remove_special(vec):
	var pos = vec_to_pos(vec)
	hasSpecial[pos[0]][pos[1]] = false

func update_graph_diags(graph, i, j):
	var centerId = pos_to_id([i, j])
	var blockedAxis = []
	for k in range(axisSuccs.size()):
		var vec = axisSuccs[k]
		var succ = [i + vec[0], j + vec[1]]
		if succ[0] < fieldSize.y and succ[0] >= 0 and succ[1] < fieldSize.x and succ[1] >= 0:
			if not graph.are_points_connected(centerId, pos_to_id(succ)):
				blockedAxis.append(k)
	for k in range(diagSuccs.size()):
		var vec = diagSuccs[k]
		var succ = [i + vec[0], j + vec[1]]
		if succ[0] < fieldSize.y and succ[0] >= 0 and succ[1] < fieldSize.x and succ[1] >= 0:
			if graph.are_points_connected(centerId, pos_to_id(succ)) and not inside(diagNeighs[k], blockedAxis):
				graph.disconnect_points(centerId, pos_to_id(succ))

func remove_directed(graph, from_id, to_id):
	var inOut = (from_id in graph.get_point_connections(to_id))
	var outIn = (to_id in graph.get_point_connections(from_id))
	if inOut:
		graph.disconnect_points(from_id, to_id)
	if outIn:
		graph.connect_points(to_id, from_id, false)

func remove_from_graph(vec, size, blockGraph):
	var pos = vec_to_pos(vec)
	var sizeInTiles = [int(size[0]/tileSize), int(size[1]/tileSize)]
	if blockGraph:
		for i in range(pos[0] + floor(-sizeInTiles[0]/2), pos[0] + floor(sizeInTiles[0]/2), 1):
			for j in range(pos[1] + floor(-sizeInTiles[1]/2), pos[1] + floor(sizeInTiles[1]/2), 1):
				var id = pos_to_id([i, j])
				for succ in get_successors(i, j):
					remove_directed(as, pos_to_id(succ), id)
					update_graph_diags(as, succ[0], succ[1])
		clear_fields()
	for i in range(pos[0] + floor(-sizeInTiles[0]/2), pos[0] + floor(sizeInTiles[0]/2), 1):
		for j in range(pos[1] + floor(-sizeInTiles[1]/2), pos[1] + floor(sizeInTiles[1]/2), 1):
			hasTower[i][j] = true

func remove_from_aux_graph(vec, size):
	var pos = vec_to_pos(vec)
	var sizeInTiles = [int(size[0]/tileSize), int(size[1]/tileSize)]
	for i in range(pos[0] + floor(-sizeInTiles[0]/2), pos[0] + floor(sizeInTiles[0]/2), 1):
		for j in range(pos[1] + floor(-sizeInTiles[1]/2), pos[1] + floor(sizeInTiles[1]/2), 1):
			var id = pos_to_id([i, j])
			for succ in get_successors(i, j):
				remove_directed(auxAs, pos_to_id(succ), id)
				update_graph_diags(auxAs, succ[0], succ[1])

func restore_aux_graph(vec, size):
	var pos = vec_to_pos(vec)
	var sizeInTiles = [int(size[0]/tileSize) + 2, int(size[1]/tileSize) + 2]
	for i in range(pos[0] + floor(-sizeInTiles[0]/2), pos[0] + floor(sizeInTiles[0]/2), 1):
		for j in range(pos[1] + floor(-sizeInTiles[1]/2), pos[1] + floor(sizeInTiles[1]/2), 1):
			var id = pos_to_id([i, j])
			for succ in get_successors(i, j):
				var otherId = pos_to_id(succ)
				var inOut = (otherId in as.get_point_connections(id))
				var outIn = (id in as.get_point_connections(otherId))
				var auxInOut = (otherId in auxAs.get_point_connections(id))
				var auxOutIn = (id in auxAs.get_point_connections(otherId))
				if outIn and not auxOutIn and inOut and not auxInOut:
					auxAs.connect_points(id, otherId)
				elif inOut and not auxInOut:
					auxAs.connect_points(id, otherId, false)
				elif outIn and not auxOutIn:
					auxAs.connect_points(otherId, id, false)

func get_chest_field(vec, silent=false):
	var pos = vec_to_pos(vec)
	var outVec = Vector2(-signRange(pos[1], 0, fieldSize.x), -signRange(pos[0], 0, fieldSize.y))
	if outVec:
		return outVec.normalized()
	if not chestField[pos[0]][pos[1]] and not silent:
		update_field(chestField, vec, chest.position)
	return chestField[pos[0]][pos[1]]

func get_spawn_field(vec, spawnerId, silent=false):
	var pos = vec_to_pos(vec)
	var field = spawnFields[spawnerId]
	if not is_inside_field(pos[0], pos[1]):
		return (spawnerList[spawnerId].position - vec).normalized()
	if not field[pos[0]][pos[1]] and not silent:
		update_field(field, vec, spawnerList[spawnerId].clampedPos)
	return field[pos[0]][pos[1]]

func has_tower(vec):
	var pos = vec_to_pos(vec)
	return hasTower[pos[0]][pos[1]]

func _on_WaveTimer_timeout():
	var wave = waves[currentWave]
	var currentTime = wave["times"][currentMonster]
	while currentMonster < wave["monsters"].size() and wave["times"][currentMonster] == currentTime:
		spawnerList[randi()%spawnerList.size()].spawn(wave["monsters"][currentMonster])
		currentMonster += 1
	if currentMonster == wave["monsters"].size():
		currentWave += 1
		currentMonster = 0
		$WaveTimer.wait_time = 0.01
	else:
		$WaveTimer.wait_time = wave["times"][currentMonster] - currentTime
		$WaveTimer.start()
	
func getMoney(amount):
	var qnt = chest.remove_money(amount)
	if qnt < amount:
		qnt = 2*(amount - qnt)
		var removed = slime.getMoney(qnt)
		if (removed < qnt):
			$GameOver.appear()
			print("GAME OVER!")
			get_tree().paused = true

func returnMoney(amount):
	chest.add_money(amount)

func can_put_tower(vec):
	var pos = vec_to_pos(vec)
	for i in range(pos[0] - 1, pos[0] + 1, 1):
		for j in range(pos[1] - 1, pos[1] + 1, 1):
			if hasTower[i][j] or not is_inside_field(i, j):
				return false
	remove_from_aux_graph(vec, [32, 32])
	var chestId = vec_to_id(chest.position)
	for spawner in spawnerList:
		var spawnerId = vec_to_id(spawner.clampedPos)
		var path = auxAs.get_id_path(spawnerId, chestId)
		if path.size() == 0:
			restore_aux_graph(vec, [32, 32])
			return false
	restore_aux_graph(vec, [32, 32])
	return true

func _on_NewWaveTimer_timeout():
	$HUD.change_wave(currentWave + 1)
	$WaveTimer.start()
