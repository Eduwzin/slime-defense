extends Control

onready var par = get_parent()

func _ready():
	pass

func _process(delta):
	update()

func _draw():
	for i in range(par.fieldSize.y):
		for j in range(par.fieldSize.x):
			var pos = Vector2(par.tileSize/2 + j*par.tileSize, par.tileSize/2 + i*par.tileSize)
			var field = par.get_chest_field(pos, true)
			var field2 = par.get_spawn_field(pos, 0, true)
			var blocked = par.has_tower(pos)
			var color = Color(1, 1, 0) if blocked else Color(1, 0, 0)
			if field != Vector2() or field2 != Vector2() or blocked:
				draw_rect(Rect2(pos - Vector2(2, 2), Vector2(4, 4)), color, true)
			if field != Vector2():
				draw_line(pos, pos + 10*field, Color(0, 1, 0))
			if field2 != Vector2():
				draw_line(pos, pos + 10*field2, Color(0.2, 0.2, 1))