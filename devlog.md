## 12/08 - 18/08

* Aprendizado

## 19/08 - 25/08

* Menu principal simples (sem sprites)
* Tela de seleção de fases simples (sem sprites)
* Cena base para as fases
* Tilemap simples (com sprites básicas)
* Criação de recursos básicos
* Criação do character (Slime) com movimentação

## 26/08 - 01/09

* Criação do baú de tesouros e de um campo para a IA dos monstros se orientarem
* Interação da Slime com os recursos
* Aprimoramento das sprites dos recursos

## 02/09 - 08/09

* Continuação do baú de tesouros
* Criação das sprites do goblin, da statue, do bug e do bird
* Criação da parte de movimentação da sprite da slime
* Criação de tileset genérico e um stage
* Mudança na forma de coleta de recursos (tecla espaço)

## 09/09 - 15/09

* Adição do primeiro monstro no jogo, junto com spawner
* Modificação do campo de orientação de IAs para o método do mapa de Dijkstra
* Modificação de algumas sprites
* Criação de um HUD, para selecionar "summons" e ver a descrição de objetos

## 16/09 - 22/09

* Criação de um tileset unificado
* Conserto de bugs
* Criação da parte de ação da sprite da slime
* Modificação de algumas sprites
* Ação de invocar um summon

## 23/09 - 29/09

* Sprite básica para torre
* Alterações no menu hud

## 30/09 - 06/10

* Sprite da torre um pouco aprimorada
* Lista de targets para a torre
* Lista de recursos para a slime
* Slime pode destruir torre invocada

## 07/10 - 13/10

* Fazer com que os monstros desviem de obstáculos
* Aprimoramento de sprites
* Slime pode invocar dois novos summons: ShootBuddy e SwordBuddy

## 14/10 - 20/10

* Criação da sprite do SleepingBuddy
* Conserto de bugs
* Término da sprite da StoneTower
* Criação de bullets
* Ajustes no menu HUD: descrição correta quando passa o mouse em algum item do menu.
* Animação da torre

## 21/10 - 27/10

* Conserto de bugs
* Criação da sprite do SwordBuddy
* Ícones no menu HUD
* Ajuste na área de ação da slime e dos recursos
* Novo recurso (moeda)

## 28/10 - 03/11

* Novos monstros foram adicionados no jogo: statue, bird e bug
* Adição da animação de ataque à sprite do SwordBuddy
* Implementação do SwordBuddy no jogo
* Implementação de projétil com dano em área
* Adição de novo recurso (lava/fire) e ajustes correspondentes no HUD e Slime
* Monstros pegam uma quantia efetiva de dinheiro do chest e da Slime (se não houver suficiente no chest)
* Aprimoramento do sistema de spawn, adicionando algumas variáveis aleatórias
* Criação de um ataque especial que reduz a velocidade de movimento dos inimigos
* Foi criado um indicador para mostrar onde as torres serão colocadas

## 04/11 - 10/11

* Criação de menu de pause durante o jogo
* Criação da sprite do baú
* Refatoração de código
* Conserto de bugs
* Criação de novos níveis

## 11/11 - 17/11
* Implementação da torre de fogo
* Proibir o jogador de colocar torres que irão bloquear completamente o caminho dos monstros até o baú
* Adição de um background para a tela de seleção de fases

## 18/11 - 24/11
* Conserto de bugs
* Implementação de dano por queimadura nos monstros
* Adição de tempo extra entre as ondas de monstros, para que o jogador possa se preparar
* Implementação de persistência no jogo
* Mudança no design de alguns menus
* O jogador agora pode cancelar a construção de torres apertando X
* Balanceamento do jogo, incluindo custos nos summons
* Logo inicial na tela de menu
* Seção de Controles na tela de menu

## 25/11 - 01/12
* Conserto de bugs
* Criação do estágio de lava
* Seção de créditos na tela de menu
* Tela de vitória
* Recursos temáticos para o estágio de lava
* Melhoramento do logo
